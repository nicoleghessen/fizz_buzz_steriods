# Here we need to make fizzbuzz functions

# Function to test if number is divisible by 3

# we need to first define the function. check_factor3
# next, need to take in one argument
# needs to assert if that argument is divisible by 3
# if it is return, true, else return false

def check_factor3(number):
   if number %3 == 0:
    return(True)
   else:
     return(False)

# print(check_factor3(6))

# print(type(check_factor3(6)))


def check_factor5(number):
    if number %5 == 0:
        return(True)
    else:
        return(False)

# print(check_factor5(20))
# print(type(check_factor5(20)))


def check_factor3_5(number):
    if check_factor3(number) and check_factor5(number):
        return(True)
    else:
        return(False)

# print(check_factor3_5(19))
# print(type(check_factor3_5(15)))



def fizzbuzz_game(number):
    if check_factor3_5(number):
        return("Fizzbuzz")
    elif check_factor5(number):
        return("Buzz")
    elif check_factor3(number):
        return("Fizz")
    else:
        return(number)

# print(fizzbuzz_game(18))