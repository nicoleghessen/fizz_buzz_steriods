from fizzbuzz_functions import *


# we want our game to run continously until we say the magic word (pineapple)
# we want the game to prompt us for input and then play the game fizzbuzz.
# if we say pineapple it exits.

while True:
    user_input=input("Give me a number pls: ")
    if user_input == "pineapple":
        break
    print(fizzbuzz_game(int(user_input)))

