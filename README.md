# FizzBuzz on Steriods

We're going to do fizzbuzz but add testing and CI pipeline.

We'll start by making some unit tests, and employ Test Driven Development. Add these tests to our pipelin. Then just focus on the code!

Things we'll cover:
- Git and Markdown
- Python functional programming
- TDD (Test driven development)
- CI (continous integration)
- Mini scrum project (without board- sprints, user stories, acceptance criteria and DoD)




### Definition of Done

TO DO.... what things do we need to check that are communal to all user stories.
- all acceptance criteria passed 
- 

### User Stories

**Game rules**
if it is a multiple of 3 --> fizz
if a multiple of 5 --> buzz
multiple of 3 and 5 --> fizzbuzz

**Core Game Logic User Stories** 

**User Story 1** As a player, when we give the game a multiple of 3 the game should output fizz, so that it follows the game logic.

**User Story 2** As a player, when we give the game a multiple of 5 the game should output buzz, so that it follows the game logic.

**User Story 3** As a player, when we give the game a multiple of 3 and 5 the game should output fizzbuzz, so that it follows the game logic.

**User Stroy 4** As a player, when we give a number that is not a multiple of 3 or 5, the game should output the number back so it follows the games logic.


**Extra User Stories To Come Later**


- As a player, I should be prompted for an input, to start the game.

- As a player, I should see all the numbers up to and including the number I chose while following the rules of the game. (fizz and buzz)

- As a player, I should be able to keep playing until I use the word: 'exit' or something you define.




### Plan/ Psudo code

TDD plan + add CI pipeline

1) write tests 
2) write code
3) make tests pass 
4) move onto the setup of a CI pipeline


**Step 1- write tests**
- use the user stories to write tests 
- write tests using 


Start with User Story 1-3

We'll need these 3 tests and functions:

- `check_factor3` -- returns True or False
- `check_factor5` -- returns True or False
- `check_factor3_5` -- returns True or False

Note: use pytest for function


then implement these functions 


## Best practises in python

1) Dry code (Don't repeat yourself... Use functions to have repition in your code. e.g. check15 calls check 3 and check5).
2) Don't print in functions because the final output is none. what this means is that if we try to test it, the output will always be none and no tests would pass. 
3) Seperation of concerns and clean code, sepereate define, run and tests. Segregate your code per sections e.g. definition of function, running of function and test. In the file with the definition of functions, don't call the functions else when you import the functions into your run functions file- you will also be having a lot of noise which is unneccesary. 
4) Keep functions small- this makes them testable and reusable 

